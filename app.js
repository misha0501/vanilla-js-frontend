const createError = require('http-errors')
const express = require('express')
const logger = require('morgan')
const cors = require('cors')

const productsRouter = require('./src/routes/products')
const bidsRouter = require('./src/routes/bids')
const authRouter = require('./src/routes/auth')
const usersRouter = require('./src/routes/users')

const app = express()

app.use(cors())
app.set('view engine', 'html')
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({
  extended: false
}))
app.use(express.static('src/static'));

app.use('/products', productsRouter)
app.use('/bids', bidsRouter)
app.use('/auth', authRouter)
app.use('/users', usersRouter)

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404))
})

// error handler
app.use((error, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = error.message
  res.locals.error = req.app.get('env') === 'development' ? error : {}

  // render the error page
  res.status(error.status || 500)
  res.render('error')
})

module.exports = app
