const fs = require('fs')

exports.getData = () => {
    return new Promise((resolve, reject) => {
        fs.readFile('./data.json', 'utf8', (error, data) => {
            if (error) {
                return reject(error)
            }

            resolve(JSON.parse(data))
        })
    })
}

exports.changeData = (data) => {
    return new Promise((resolve, reject) => {
        fs.writeFile('./data.json', JSON.stringify(data), error => {
            if (error) {
                return reject(error)
            }

            resolve()
        })
    })
}