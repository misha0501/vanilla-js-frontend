# Assignment 2 (Vanilla JS frontend)
## How frontend and backend interact with each other?
In this assignment we have created "sendJSON" method in util.js file. When executed, it uses native xml http requet functionality to interact with the server. If user is logged in, additionaly we send **Authorization** header with value **Bearer {token}**. Token is stored in local storage.

## How authorization works?
After user send login/register request and it is successful, server send a response with encrypted token. We save it in local storage. After, the user is redirect to the home page and now don't see login route at the header. Now he see new tabs and able to make request's which only users can make. 
## What libraries do we use?
We have not used any libraries because it is forbidden.
