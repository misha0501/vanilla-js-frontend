const express = require('express')

const isAdmin = require('../middleware/admin')
const isAuthed = require('../middleware/auth')

const {
  getData,
  changeData
} = require('../../dataProvider.js')

const router = express.Router()

router.get('/', async (req, res, next) => {
  let {
    nameSearch,
    infoSearch,
    categorySearch
  } = req.query

  let result = []
  try {
    let {products} = await getData()

    if (nameSearch || infoSearch || categorySearch) {
      if (nameSearch) nameSearch = nameSearch.toLowerCase()
      if (infoSearch) infoSearch = infoSearch.toLowerCase()
      if (categorySearch) categorySearch = categorySearch.toLowerCase()

      if (!nameSearch || !nameSearch.length) nameSearch = null
      if (!infoSearch || !infoSearch.length) infoSearch = null
      if (!categorySearch || !categorySearch.length) categorySearch = null

      products.forEach(product => {
        if (product && product.name) {
          let name = product.name.toLowerCase()
          let info = product.info.toLowerCase()
          let category = product.category.toLowerCase()

          console.log(category, categorySearch, category.indexOf(categorySearch) !== -1)
          console.log(name, nameSearch, name.indexOf(nameSearch) !== -1)
          console.log(info, infoSearch, info.indexOf(infoSearch) !== -1)

          if (name.indexOf(nameSearch) !== -1 || info.indexOf(infoSearch) !== -1 || category.indexOf(categorySearch) !== -1) {
            result.push(product)
          }
        }
      })
    } else {
      products = products.filter(product => {
        if (product) {
          return product
        }
      })
      result = products
    }
  } catch (error) {
    console.log(error)
    res.status(500).json({
      success: false,
      message: error
    })
    return
  }


  res.json({
    success: true,
    message: result
  })
})

router.get('/:id', async (req, res, next) => {
  const {
    id
  } = req.params

  let result = null
  let data
  try {
    data = await getData()
  } catch (error) {
    res.json({
      success: false,
      message: error
    })
    return
  }

  for (let i = 0; i < data.products.length; i++) {
    if (data.products[i] && data.products[i].productId == id) {
      result = data.products[i]
    }
  }

  res.json({
    success: true,
    message: result
  })
})

router.post('/', [isAuthed, isAdmin], async (req, res, next) => {
  const {
    name,
    category,
    info,
    auctionEnds
  } = req.body

  let data
  try {
    data = await getData()
  } catch (error) {
    console.log(error)
    res.json({
      success: false,
      message: error
    })
    return
  }

  const product = {
    productId: data.products.length + 1,
    name,
    category,
    info,
    buyer: null,
    auctionEnds
  }
  data.products.push(product)

  try {
    await changeData(data)
  } catch (error) {
    console.log(error)
    res.json({
      success: false,
      message: error
    })
    return
  }
  res.send(product)
})

router.put('/', [isAuthed, isAdmin], async (req, res, next) => {
  let {
    productId,
    name,
    category,
    info,
    auctionEnds,
    buyer
  } = req.body

  let data
  try {
    data = await getData()
  } catch (error) {
    res.json({
      success: false,
      message: error
    })
    return
  }

  for (let i = 0; i < data.products.length; i++) {
    if (data.products[i] && data.products[i].productId == productId) {
      data.products[i] = {
        productId,
        name,
        category,
        info,
        buyer,
        auctionEnds
      }
    }
  }

  try {
    await changeData(data)
  } catch (error) {
    res.json({
      success: false,
      message: error
    })
    return
  }
  res.send({
    success: true
  })
})

router.delete('/', [isAuthed, isAdmin], async (req, res, next) => {
  const {
    productId
  } = req.body

  let data
  try {
    data = await getData()
  } catch (error) {
    res.json({
      success: false,
      message: error
    })
    return
  }

  for (let i = 0; i < data.products.length; i++) {
    if (data.products[i] && data.products[i].productId == productId) {
      delete data.products[i]
    }
  }

  try {
    await changeData(data)
  } catch (error) {
    res.json({
      success: false,
      message: error
    })
    return
  }
  res.json({
    success: true,
    message: "Product was deleted."
  })
})

module.exports = router
