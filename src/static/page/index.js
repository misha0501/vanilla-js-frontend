// TODO: control index page

import { sendJSON, saveToken, validateInputControl } from './util.js'

import { handleNav }  from './navigationHandler.js'

const PER_PAGE = 3
let productsCache = []

const getFromPage = page => {
  document.getElementById('auctionList').innerHTML = ''
  let from = page * PER_PAGE - PER_PAGE
  let to = page * PER_PAGE

  let elements = document.getElementsByClassName('pageclick')
  Array.from(elements).forEach(element => {
    const pageEl = element.dataset.page

    console.log(page, pageEl)
    if (page === pageEl) {
      element.classList.add('active')
    } else if (element.classList.contains('active')) {
      element.classList.remove('active')
    }
  })
  
  for (let i = 0; i < productsCache.length; i++) {
    if (i + 1 > from && i + 1 <= to) {
      const item = productsCache[i]
      if (!item) return
      let date = ''
      if (Number(item.auctionEnds) !== 0) {
        const dateObj = new Date()
        dateObj.setTime(item.auctionEnds)
        date = `${dateObj.getFullYear()}-${dateObj.getMonth()}-${dateObj.getDate()}`
      }

      sendJSON({
        method: 'GET',
        url: `http://localhost:3000/bids/product/${item.productId}`
      }, (error, body) => {
        if (!error) {
          let highestBid = 0

          body.message.forEach(bid => {
            if (Number(bid.amount) > highestBid) {
              highestBid = Number(bid.amount)
            }
          })

          document.getElementById('auctionList').insertAdjacentHTML('afterbegin', `
            <section class="auction_box">
                <a class="auction_title" href="auction.html?id=${item.productId}">${item.name}</a>
                <p class="auction_description">
                    ${item.info}
                </p>
                <div class="auction_bid">
                    <span class="auction_bid_price">${highestBid}</span>
                    <span class="auction_bid_time">${date}</span>
                </div>
            </section>
          `)
        }
      })
    }
  }
}

window.onload = () => {
  handleNav();

  const url_string = window.location.href
  const url = new URL(url_string)
  const nameSearch = url.searchParams.get('nameSearch')

  let params = {
    nameSearch: url.searchParams.get('nameSearch'),
    categorySearch: url.searchParams.get('categorySearch'),
    infoSearch: url.searchParams.get('infoSearch')
  }

  let reqUrl = 'http://localhost:3000/products?'
  if (nameSearch) {
    document.querySelector("body > nav > div > form > input[type=text]").value = nameSearch
  }

  for (const key in params) {
    document.querySelector(`input[name="${key}"]`).value = params[key] || ''
    reqUrl += `${key}=${params[key] || ''}&`
  }

  sendJSON({
    method: 'GET',
    url: reqUrl
  }, (error, body) => {
    if (!error) {
      let pages = 1
      try {
        pages = Math.round(body.message.length / PER_PAGE)
      } catch {}

      for (let i = 0; i < pages; i++) {
        document.getElementById('pages').insertAdjacentHTML('beforeend', `
          <a href="#" class="pageclick" data-page="${i + 1}">${i + 1}</a>
        `)
      }

      let elements = document.getElementsByClassName('pageclick')
      Array.from(elements).forEach(element => {
        const { page } = element.dataset
        element.addEventListener('click', function () {
          getFromPage(page)
        }, false)
      })

      productsCache = body.message
      getFromPage(1)
    }
  })
}
