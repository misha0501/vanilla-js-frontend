// import utilities from util.js
import {
    sendJSON,
    saveToken,
    validateInputControl
} from './util.js'
import {
    handleNav
} from './navigationHandler.js'

handleNav()

// grab form controls from the DOM
const
    form = document.querySelector('main form'),
    usernameField = form.querySelector('input[name="username"]'),
    emailField = form.querySelector('input[name="email"]'),
    passwordField = form.querySelector('input[name="password"]'),
    repeatPasswordField = form.querySelector('input[name="password_repeat"]'),
    registerButton = form.querySelector('input[type="submit"]'),
    errorMessage = form.querySelector('.error_message')

const validateEmail = email => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(String(email).toLowerCase())
}

const hasNumber = string => {
    return /\d/.test(string)
}

// respond to click event on login button
registerButton.addEventListener('click', event => {
    // do not submit form (the default action of a submit button)
    event.preventDefault()
    // construct request body with username and password
    const body = {
        login: usernameField.value,
        password: passwordField.value,
        email: emailField.value
    }

    if (passwordField.value !== repeatPasswordField.value) {
        errorMessage.style.display = "block"
        errorMessage.innerHTML = "Password does not match"
        return
    }
    if (!validateEmail(emailField.value) || (!emailField.value.includes('.nl') && !emailField.value.includes('.com'))) {
        errorMessage.style.display = "block"
        errorMessage.innerHTML = "E-Mail adress is incorrect"
        return
    }
    if (passwordField.value.toLowerCase() === passwordField.value || passwordField.value.length < 6 || !hasNumber(passwordField.value)) {
        errorMessage.style.display = "block"
        errorMessage.innerHTML = "Password must be from 6 characters, contain at least 1 number and capital letter"
        return
    }
    // send PUT request with body to /credentials and wait for HTTP response
    sendJSON({
        method: 'POST',
        url: '/users',
        body
    }, (err, response) => {
        // if err is undefined, the send operation was a success
        if (!err) {
            // store token from response body if login is a success
            saveToken(response.message)
            window.location.href = "http://localhost:3000/bids.html"

        } else {
            errorMessage.style.display = "block"
            errorMessage.innerHTML = response.message;
            console.error(err)
        }
    })
})

// validate login form
function validatePassword() {
    const repeatErrorMessage = document.querySelector(".input__error__message");
    if (passwordField.value && repeatPasswordField.value) {
        if (passwordField.value !== repeatPasswordField.value) {
            repeatErrorMessage.style.display = "block";
        } else {
            repeatErrorMessage.style.display = "none";
        }

        return passwordField.value === repeatPasswordField.value;
    }
}

function validateForm() {
    const
        usernameOk = usernameField.value.length > 0,
        passwordOk = passwordField.value.length > 0,
        emailOk = emailField.value.length > 0,

        registerOk = usernameOk && passwordOk && emailOk && validatePassword()
    // provide visual feedback for controls in a 'bad' state
    validateInputControl(usernameField, usernameOk)
    validateInputControl(passwordField, passwordOk)
    validateInputControl(emailField, emailOk)
    validateInputControl(repeatPasswordField, validatePassword())

    // enable/disable click of register button
    registerButton.disabled = !registerOk
}

// validate form on every input event
form.addEventListener('input', validateForm)

// validate form on page load
// validateForm()