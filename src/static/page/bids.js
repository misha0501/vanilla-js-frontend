import {handleNav} from './navigationHandler.js'
import {sendJSON, currentUserLogin, getTime} from "./util.js";

handleNav()

function deleteBid(id) {
  sendJSON({
    method: 'DELETE',
    url: `http://localhost:3000/bids`,
    body: {
      bidId: id
    }
  }, (error, body) => {
    getAllBids()
  })
}

const getAllBids = () => {
  sendJSON({
    method: 'GET',
    url: `http://localhost:3000/bids`
  }, (error, body) => {
    if (!error && body.message) {
      loadInfo(body.message)
    }
  })
}

getAllBids()


function loadInfo(bids) {
  document.querySelector("html body main div.row table tbody").outerHTML = `<tr>
      <th>Name</th>
      <th>Price</th>
      <th>Time</th>
      <th>Is best bid?</th>
      <th>Remove</th>
  </tr>`
  bids = bids.filter(bid => {
    if (bid && bid.userLogin === currentUserLogin()) {
      return bid
    }
  })
  console.log(bids)
  bids.forEach((bid, i) => {
    sendJSON({
      method: 'GET',
      url: `http://localhost:3000/products/${Number(bid.productId)}`
    }, (error, body) => {
      if (!error && body.message) {
        name = body.message.name

        document.querySelector("html body main div.row table tbody").insertAdjacentHTML('beforeend', `
            <tr>
              <td>${name}</td>
              <td>${bid.amount}</td>
              <td>${getTime(bid.createdAt)}</td>
              <td>${isBestBid(bid, bids) ? "Yes" : "No"}</td>
              <td><i class="fa fa-trash deleteBid" data-id="${bid.bidId}" ></i></td>
          </tr>
        `)

        if (i === bids.length - 1) {
          const elements = document.getElementsByClassName("deleteBid")
          Array.from(elements).forEach(element => {
            const {id} = element.dataset
            element.addEventListener('click', function () {
              deleteBid(id)
            }, false)
          })
        }
      }
    })
  })
}


function isBestBid(bid, bids) {
  const userBidAmount = bid.amount;
  console.log(userBidAmount)
    let result = true;

  bids.forEach(bidFromArray => {
      console.log(userBidAmount)

      if (bidFromArray && bidFromArray.productId === bid.productId && bid.userLogin === bidFromArray.userLogin && bid.bidId !== bidFromArray.bidId && Number(bidFromArray.amount) > Number(userBidAmount) ) {
      result = false;
    }
  })

  return result;
}