// TODO: control administration page
import { handleNav }  from './navigationHandler.js'
import {sendJSON} from "./util.js";

handleNav()

const loadAuctions = () => {
  sendJSON({
    method: 'GET',
    url: 'http://localhost:3000/products'
  }, (error, body) => {
    if (!error) {
      document.querySelector("body > main > div:nth-child(2) > table > tbody").innerHTML = ''
      body.message.forEach((item, i) => {
        if (!item) return
        let date = ''
        if (Number(item.auctionEnds) !== 0) {
          const dateObj = new Date()
          dateObj.setTime(item.auctionEnds)
          date = `${dateObj.getFullYear()}-${dateObj.getMonth()}-${dateObj.getDate()}`
        }

        document.querySelector("body > main > div:nth-child(2) > table > tbody").insertAdjacentHTML('afterbegin', `
          <tr>
            <td>${item.name}</td>
            <td>${date}</td>
            <td>
              <i class="fa fa-pencil editProduct" data-id="${item.productId}"></i>
              <i class="fa fa-trash deleteProduct" data-id="${item.productId}"></i>
            </td>
          </tr>
        `)
      })

      let elements = document.getElementsByClassName("editProduct")
      Array.from(elements).forEach(element => {
        const { id } = element.dataset
        element.addEventListener('click', function () {
          editProduct(id)
        }, false)
      })
      elements = document.getElementsByClassName("deleteProduct")
      Array.from(elements).forEach(element => {
        const { id } = element.dataset
        element.addEventListener('click', function () {
          deleteProduct(id)
        }, false)
      })

      document.querySelector("body > main > div:nth-child(2) > table > tbody").insertAdjacentHTML('afterbegin', `
        <tr>
          <th>Name</th>
          <th>End time</th>
          <th>Actions</th>
        </tr>
      `)
    }
  })
}

const deleteProduct = id => {
  sendJSON({
    method: 'DELETE',
    url: 'http://localhost:3000/products',
    body: {
      productId: id
    }
  }, () => {
    loadAuctions()
  })
}

let currentId
const editProduct = id => {
  currentId = id
  sendJSON({
    method: 'GET',
    url: 'http://localhost:3000/products'
  }, (error, body) => {
    if (!error) {
      body = JSON.parse(body)
      let products = body.message.filter(product => {
        if (product) {
          return product
        }
      })
      products.forEach(product => {
        if (product.productId == id) {
          document.getElementsByClassName('editAuction')[0].classList.remove('hidden')

          document.getElementById('edit-name').value = product.name
          document.getElementById('edit-category').value = product.category
          document.getElementById('edit-description').value = product.info
          document.getElementById('edit-end_time').value = product.auctionEnds
        }
      })
    }
  })
}

window.onload = () => {
  loadAuctions()

  const form = document.querySelector("body > main > div:nth-child(4) > form")

  form.addEventListener('submit', event => {
    event.preventDefault()

    const name = document.querySelector("body > main > div:nth-child(4) > form > input[type=text]:nth-child(2)").value
    const category = document.querySelector("body > main > div:nth-child(4) > form > input[type=text]:nth-child(3)").value
    const info = document.querySelector("body > main > div:nth-child(4) > form > input[type=text]:nth-child(4)").value
    const auctionEnds = document.querySelector("body > main > div:nth-child(4) > form > input[type=text]:nth-child(5)").value

    sendJSON({
      method: 'POST',
      url: 'http://localhost:3000/products',
      body: {
        name,
        category,
        info,
        auctionEnds
      }
    }, () => {
      loadAuctions()
    })
  })

  document.getElementById('editAuctionForm').onsubmit = e => {
    e.preventDefault()

    const name = document.getElementById('edit-name').value
    const category = document.getElementById('edit-category').value
    const info = document.getElementById('edit-description').value
    const auctionEnds = document.getElementById('edit-end_time').value

    sendJSON({
      method: 'PUT',
      url: 'http://localhost:3000/products',
      body: {
        productId: currentId,
        name,
        category,
        info,
        auctionEnds,
        buyer: null
      }
    }, () => {
      loadAuctions()
    })
  }
}