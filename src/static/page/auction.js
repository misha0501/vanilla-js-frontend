// TODO: control auction page
import { handleNav }  from './navigationHandler.js'
import {sendJSON, isLoggedIn, getTime} from "./util.js";

handleNav()

function handleBidField(bidAmount, bidButton) {
  if (!isLoggedIn()) {
    bidButton.style.display = "none"
    bidAmount.style.display = "none"
  }
}

window.onload = () => {
  const bidAmount = document.querySelector(".auction_bid_amount");
  const bidButton= document.querySelector(".auction_bid_button");

  handleBidField(bidAmount, bidButton);

  const url_string = window.location.href
  const url = new URL(url_string)
  const id = url.searchParams.get('id')

  sendJSON({
    method: 'GET',
    url: `http://localhost:3000/products/${id}`
  }, (error, body) => {
    if (!error && body.message) {
      document.querySelector("body > main > div > section > div:nth-child(2) > h1").textContent = body.message.name
      document.querySelector("body > main > div > section > div:nth-child(2) > p").textContent = body.message.info

      loadBids(id)
    }
  })

  const form = document.querySelector("body > main > div > section > div:nth-child(2) > form")

  form.addEventListener('submit', event => {
    event.preventDefault()

    sendJSON({
      method: 'POST',
      url: 'http://localhost:3000/bids',
      body: {
        productId: id,
        amount: Number(document.querySelector("body > main > div > section > div:nth-child(2) > form > input.auction_bid_amount").value)
      }
    }, (error, response) => {
      if (!response.success) {
        document.querySelector("body > main > div > section > div:nth-child(2) > p").innerHTML = response.message;

        document.querySelector("body > main > div > section > div:nth-child(2) > p").style.display = "block";
        document.querySelector("body > main > div > section > div:nth-child(2) > p").style.color = "red";
      } else {
        document.querySelector("body > main > div > section > div:nth-child(2) > p").style.display = "none";
        loadBids(id);

      }
    })
  })
}

const loadBids = id => {
  sendJSON({
    method: 'GET',
    url: `http://localhost:3000/bids/product/${id}`
  }, (error, body) => {
    if (!error) {
      document.querySelector("body > main > div > section > div:nth-child(2) > div > ul").innerHTML = ""

      body.message.sort((a, b) => {
        return a.amount - b.amount
      })

      body.message.forEach(bid => {
        document.querySelector("body > main > div > section > div:nth-child(2) > div > ul").insertAdjacentHTML('afterbegin', `
              <li class="auction_detail_bid">
                <span class="auction_detail_bid_price">$${bid.amount}</span>
                <span class="auction_detail_bid_user">${bid.userLogin ? bid.userLogin : 'Unknown'}</span>
                <span class="auction_detail_bid_time">${getTime(bid.createdAt)}</span>
              </li>
            `)
      })
    }
  })
}

// const getTime = timestamp => {
//   const date = new Date(timestamp)
//   return `${date.getDate()}-${date.getMonth() + 1} ${date.getHours()}:${date.getMinutes()}`
// }