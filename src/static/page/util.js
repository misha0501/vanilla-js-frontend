// JWT token string with header, payload and signature
let sessionToken = window.localStorage.getItem("Token")

// send HTTP request, possibly with JSON body, and invoke callback when JSON response body arrives
export function sendJSON({ method, url, body }, callback) {
    const xhr = new XMLHttpRequest()
    xhr.addEventListener('load', () => {
        if (xhr.status === 200 || xhr.status === 304) {
            try {
                callback(undefined, JSON.parse(xhr.responseText))
            } catch {
                callback(undefined, xhr.responseText)
            }
        } else {
            callback(new Error(xhr.statusText), JSON.parse(xhr.responseText))
        }
    })
    xhr.open(method, url)
    xhr.setRequestHeader('Content-Type', 'application/json')

    if (sessionToken) {
        xhr.setRequestHeader('Authorization', `Bearer ${sessionToken}`)
    }
    xhr.send(body !== undefined ? JSON.stringify(body) : undefined)
}

export function saveToken({token}) {
    if (token) {
        sessionToken = token;
        window.localStorage.setItem("Token", token);
    }

}

export function resetToken() {
    // clear token when users logs out
    sessionToken = undefined

    localStorage.removeItem("Token");
}

export function getTokenPayload() {
    if (sessionToken) {
        // extract JSON payload from token string
        return JSON.parse(atob(sessionToken.split('.')[1]))
    }
    return undefined
}

export function currentUserLogin() {
    const tokenPayload = getTokenPayload();
    if (tokenPayload) {
        // extract JSON payload from token string
        return tokenPayload.login;
    }
    return undefined
}

// utility functions adds/removes CSS class 'bad' upon validation
export function validateInputControl(element, ok) {
    if (ok) {
        element.classList.remove('bad')
    } else {
        element.classList.add('bad')
    }
}

export function isLoggedIn() {
    return !!sessionToken;
}

export const getTime = timestamp => {
    const date = new Date(timestamp)
    return `${date.getDate()}-${date.getMonth() + 1} ${date.getHours()}:${date.getMinutes()}`
}

export function isAdmin() {
    const tokenPayload = getTokenPayload();
    if (tokenPayload) {
        return tokenPayload.isAdmin
    };
}


