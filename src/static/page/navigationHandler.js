import {isAdmin, isLoggedIn, resetToken} from './util.js'



const body = document.querySelector("body");
const navigation = body.querySelector("nav");


navigation.addEventListener("onload", handleNav);




export function handleNav() {
    loginButtonHandler()

    logOutButtonHandler()

    administrationButtonHandler()

    myBidsButtonHandler()

}


function loginButtonHandler() {
    if (isLoggedIn()) navigation.querySelector('[href = "login.html"]').style.display = "none";
}

function logOutButtonHandler() {
    const logOutButton = navigation.querySelector('[href = "/"]')

    if (!isLoggedIn()) logOutButton.style.display = "none";

    logOutButton.addEventListener("click", resetToken);
}

function administrationButtonHandler() {
    const administrationButton = navigation.querySelector('[href = "administration.html"]')

    if (!isAdmin()) administrationButton.style.display = "none";

}

function myBidsButtonHandler() {
    if (!isLoggedIn()) navigation.querySelector('[href = "bids.html"]').style.display = "none";

}

