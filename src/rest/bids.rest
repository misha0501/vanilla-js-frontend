###
POST http://localhost:3000/auth
Content-Type: application/json
Accept: application/json

{
  "login": "admin",
  "password": "admin"
}

> {%
  client.global.set("auth_token", response.body.message.token)

  client.test("User logged in successfully", function() {client.assert(response.status === 200, "User is not logged in. Response status is not 200");});

%}

###
GET http://localhost:3000/bids
Content-Type: application/json
Accept: application/json
Authorization: Bearer {{auth_token}}

> {%

  client.test("User received his bids", function() {client.assert(response.body.success === true, "User didn't receive his bids");});

%}

###
POST http://localhost:3000/bids
Content-Type: application/json
Accept: application/json
Authorization: Bearer {{auth_token}}

{
  "productId": "0",
  "amount": "400"
}


> {%
  client.test("Bid was placed successfully", function() {client.assert(response.body.message.amount == 400 && response.body.message.productId == 0 , "Bid wasn't placed.");});
%}


###

GET http://localhost:3000/bids/product/0
Content-Type: application/json
Accept: application/json

> {%
  client.test("Bids for a certain product were retrieved successfully.", function() {client.assert(response.status === 200, "Bids for a certain product weren't retrieved.")})
%}



###
GET http://localhost:3000/bids/0
Content-Type: application/json
Accept: application/json

> {%
  client.test("A bid was retrieved successfully.", function() {client.assert(response.status === 200, "A bid wasn't retrieved ")})
%}

###
GET http://localhost:3000/bids/999999999999999999
Content-Type: application/json
Accept: application/json

> {%
  client.test("A bid with wrong id wasn't retrieved.", function() {client.assert(!response.body.message, "A bid with wrong id was retrieved.")})
%}

###
POST http://localhost:3000/auth
Content-Type: application/json
Accept: application/json

{
  "login": "admin1111",
  "password": "123adDAS"

}

> {%
  client.global.set("auth_token", response.body.message.token)

%}


###
DELETE http://localhost:3000/bids
Content-Type: application/json
Accept: application/json
Authorization: Bearer {{auth_token}}

{
  "bidId": "11"
}

> {%
  client.test("Wrong person couldn't delete a bid.", function() {client.assert(response.status === 403, "Wrong person could delete a bid")})
%}